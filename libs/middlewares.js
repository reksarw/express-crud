const jwt = require('./jwt')

module.exports = {
	HandshakeJWT: function(request, response, next) {
		let bearer = jwt.ensureToken(request)
		let isVerify = jwt.isVerify(bearer)

		if(!isVerify) {
			return response.status(403)
				.json({
					is_ok: false,
					error_message: "You\'re not authorized!"
				})
		}

		request.token = bearer
		request.payload = isVerify
		next()
	},
	logger: function(request, response, next) {
		let message = 'Hit at: ' + request.originalUrl
		console.log(message)
		next()
	}
}
