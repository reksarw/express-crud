const jwt = require('jsonwebtoken')
const config = require('../config')

module.exports = {
	createJWToken: function(payload) {
		if (typeof payload !== 'object') return false

		const token = jwt.sign(payload, config.JWT_SECRET, {
				expiresIn: '12h'
			})

		return token
	},
	ensureToken: function(request) {
		const bearerHeader = request.headers["authorization"];
  	if (typeof bearerHeader === 'undefined') return false

  	const bearer = bearerHeader.split(" ")
    const bearerToken = bearer[1]

    return bearerToken
	},
	isVerify: function(token) {
		let result = false
		jwt.verify(token, config.JWT_SECRET, function(err, payload){
			result = !err ? payload : false
		})

		return result
	}
}