let mysql = require('mysql')
let config = require('../config')

let connection = mysql.createConnection({
  host: config.DB_HOST,
  user: config.DB_USER,
  password: config.DB_PASS,
  database: config.DB_NAME
})

connection.connect(function(err) {
  if (err) {
    console.log('Something wrong with MySQL database connection')
    connection.end()
  }
})

module.exports = connection
