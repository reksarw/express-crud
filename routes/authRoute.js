const express = require('express')
const router = express.Router()
const jwt = require('../libs/jwt')
const connector = require('../libs/mysql-connector')
const helper = require('../libs/helpers')
const bcrypt = require('bcrypt')

router.get('/ping', function(request, response){
	let bearer = jwt.ensureToken(request)
	let isVerify = jwt.isVerify(bearer)

	if(!isVerify) {
		return response.status(403)
			.json({
				is_ok: false,
				error_message: "You\'re not authorized!"
			})
	}

	return response.status(200)
		.json({
			is_ok: true,
			message: "PONG!",
			token: bearer,
			payload: isVerify,
			expired_token: helper.timeConverter(isVerify.exp) 
		})
})

router.post('/login', function(request, response){
	let { email, password } = request.body
	
	if(!email || !password) {
		return response.status(422)
			.json({
				is_ok: false,
				error_message: "Invalid parameter"
			})
	}

	if(!helper.validateEmail(email)) {
		return response.status(422)
			.json({
				is_ok: false,
				error_message: "Invalid email"
			})
	}

	var sql = "SELECT * FROM m_accounts WHERE email = '"+ email +"'"
	let checkEmail = connector.query(sql, function(err, rows){
		if(err) {
			return response.status(500)
				.json({
	        is_ok: false,
	        error: err
	     })	
		}

		if(rows.length === 0) {
			return response.status(500)
				.json({
					is_ok: false,
					error_message: "Username atau Password salah!"
				})
		} else {
			let data = rows[0]
			bcrypt.compare(password, data.password, function(err, result){
				if(err) {
		     return response.status(500)
		     	.json({
			        is_ok: false,
			        error: err
			     })
		    } else {
		    	let payload = {
		    		id: data.id,
		    		email: data.email,
		    		last_login: data.last_login
		    	}

		    	// Create JWT Token
		    	let token = jwt.createJWToken(payload)

		    	// Update Last Login
					let now = new Date().toISOString().slice(0, 19).replace('T', ' ')
					var sql = 'UPDATE m_accounts SET last_login = "'+ now +'" WHERE id = "' + data.id + '"'

					connector.query(sql, function(err){
						if(err) {
							return response.status(500)
								.json({
					        is_ok: false,
					        error: err
					     })
						}
					})

					// The result
		    	return response.status(200)
		    		.json({
		    			is_ok: true,
		    			token: token
		    		})
		    }
			})
		}
	})
})

router.post('/register', function(request, response){
	let { email, password } = request.body

	if(!email || !password) {
		return response.status(422)
			.json({
				is_ok: false,
				error_message: "Invalid parameter"
			})
	}

	if(!helper.validateEmail(email)) {
		return response.status(422)
			.json({
				is_ok: false,
				error_message: "Invalid email"
			})
	}

	var sql = "SELECT * FROM m_accounts WHERE email = '"+ email +"'"
	let checkEmail = connector.query(sql, function(err, rows){
		if(err) {
			return response.status(500)
				.json({
	        is_ok: false,
	        error: err
	     })
		}

		if(rows.length === 1) {
			return response.status(500)
				.json({
					is_ok: false,
					error_message: "Email sudah digunakan!"
				})
		} else {
			bcrypt.hash(password, 10, function(err, hash){
				if(err) {
		     return response.status(500)
		     	.json({
			        is_ok: false,
			        error: err
			     })
		    } else {
		    	var sql = 'INSERT INTO m_accounts(email,password) VALUES ("'+ email +'", "'+ hash +'")'
		    	connector.query(sql, function(err, rows) {
		    		if(err) {
		    			return response.status(500)
		    				.json({
		    					is_ok: false,
		    					error_message: err
		    				})
		    		}

						return response.status(200)
							.json({
								is_ok: true,
								message: "Berhasil menambah data!"
							})
		    	})
		    }
			})
		}
	})
})

module.exports = router
