const express = require('express')
const router = express.Router()
const jwt = require('../libs/jwt')
const middlewares = require('../libs/middlewares') 
const Joi = require('joi')
const connector = require('../libs/mysql-connector')
const multer = require('multer')
const crypto = require('crypto')
const config = require('../config')
const fs = require('fs')
const mkdirp = require('mkdirp')
const path = require('path')

const addUserSchema = Joi.object().keys({
	// Example request: 13996/0678.070
	nis: Joi.string().required(),
	// Example request: Reksa Rangga Wardhana
	nama: Joi.string().min(2).max(50).required(),
  // Example request: 0823-3456-9848 atau 0823-3456-984
  no_hp: Joi.string().regex(/^\d{4}-\d{4}-\d{3,4}$/).required(),
	// Example request: Jakarta
	tempat_lahir: Joi.string().min(2).max(20).required(),
  // Example request: 1998-10-24 (Y-M-D)
  tanggal_lahir: Joi.date().max('1-1-2004').iso().required(),
})

const editUserSchema = Joi.object().keys({
	// Example request: 13996/0678.070
	nis: Joi.string(),
	// Example request: Reksa Rangga Wardhana
	nama: Joi.string().min(2).max(50),
  // Example request: 0823-3456-9848 atau 0823-3456-984
  no_hp: Joi.string().regex(/^\d{4}-\d{4}-\d{3,4}$/),
	// Example request: Jakarta
	tempat_lahir: Joi.string().min(2).max(20),
  // Example request: 1998-10-24 (Y-M-D)
  tanggal_lahir: Joi.date().max('1-1-2004').iso(),
})

const deleteUserSchema = {
	params: Joi.object({
		id: Joi.number().integer().required()
	})
}

router.all('*', middlewares.HandshakeJWT)

// Multer Config
const storage = multer.diskStorage({
  destination: (req, file, callback) => {
		var dir = config.STATIC_URL + config.IMAGE_STORAGE
		if(!fs.existsSync(dir)) {
			mkdirp(path.join(process.cwd() + '/' + dir))
		}
  	callback(null, dir)
  },
  filename: (req, file, callback) => {
		let customFileName = ''
		for(var i = 0; i < 3; i++) {
			customFileName += crypto.pseudoRandomBytes(Math.floor(Math.random() * 9)+5).toString('hex') + "-"
		}

		let fileName = customFileName.replace(/-$/i, "")
		let fileExtension = file.originalname.split('.')[1]
    callback(null, fileName + '.' + fileExtension)
  }
})
const uploadFile = multer({ storage: storage })

router.get('/', function(request, response){
	let page = request.query.page ? request.query.page * 2 : 0
	var sql = "SELECT * FROM t_siswa ORDER BY date_added DESC LIMIT ?, 3"
	
	connector.query(sql, page, (err, lists) => {
		if(err) {
			return response.status(500)
				.json({
	        is_ok: false,
	        error: err
	     })
		}

		if(lists.length === 0) {
			response.status(500)
				.json({
					is_ok: false,
					error_message: "User ID tidak ditemukan!"
				})
		} else {
			let data = []
			for(var index = 0; index < lists.length; index++) {
				let row = lists[index]
				data.push({
					id: row.id,
					nis: row.nis,
					nama_lengkap: row.nama_lengkap,
					tempat_lahir: row.tempat_lahir,
					tanggal_lahir: row.tanggal_lahir,
					no_hp: row.no_hp,
					foto: row.foto
				})
			}

			response.status(200)
				.json({
					is_ok: true,
					data: data
				})
		}
	})
})

router.get('/:id(\\d+)', function(request, response){
	var sql = "SELECT * FROM t_siswa WHERE id = ?"

	connector.query(sql, request.params.id, (err, list) => {
		if(err) {
			return response.status(500)
				.json({
	        is_ok: false,
	        error: err
	     })
		}

		if(list.length === 0) {
			response.status(500)
				.json({
					is_ok: false,
					error_message: "User ID tidak ditemukan!"
				})
		} else {
			let row = list[0]
			let data = {
				id: row.id,
				nis: row.nis,
				nama_lengkap: row.nama_lengkap,
				tempat_lahir: row.tempat_lahir,
				tanggal_lahir: row.tanggal_lahir,
				no_hp: row.no_hp,
				foto: row.foto
			}

			response.status(200)
				.json({
					is_ok: true,
					data: data
				})
		}
	})
})

router.put('/:id/uploadfoto', uploadFile.single('foto'), function(request, response){
	if (!request.file) {
    return response.status(422).json({
      is_ok: false,
      error_message: "File belum dipilih"
    })
  } else {
  	let id = request.params.id
  	var sql = "UPDATE t_siswa SET foto = ? , last_modified = ? WHERE id = ?"
  	let host = request.host;
		let filePath = request.protocol + "://" + host + ':' + config.PORT;
  	let imagePath = config.STATIC_STORAGE + config.IMAGE_STORAGE + "/" + request.file.filename
  	let fullPath = filePath + imagePath

  	let now = new Date().toISOString().slice(0, 19).replace('T', ' ')
	 	let updateCondition = [fullPath, now, id]

  	connector.query(sql, updateCondition, (err) => {
  		if(err) {
  			return response.status(500)
					.json({
		        is_ok: false,
		        error: err
		     })
  		}
  	})

    return response.status(200).json({
      is_ok: true,
      message: "Berhasil mengubah foto siswa"
    })
  }
})


router.post('/add', function(request, response){
	const data = request.body
  const payload = request.payload
	
	Joi.validate(data, addUserSchema, (err, value) => {
    if (err) {
      response.status(422).json({
        is_ok: false,
        message: 'Invalid request data'
      })
    } else {
    	var sql = "INSERT INTO t_siswa (nis,nama_lengkap, tempat_lahir, tanggal_lahir, no_hp, added_by) VALUES (?,?,?,?,?,?)"
    	dataInsert = [
    		value.nis, value.nama, value.tempat_lahir, 
    		value.tanggal_lahir, value.no_hp.replace(/-/g, ''),
    		payload.id
    	]

    	connector.query(sql, dataInsert, (err, rows) => {
    		if(err) {
	  			return response.status(500)
	  				.json({
	  					is_ok: false,
	  					error_message: err
	  				})
	  		}

	      return response.json({
	          is_ok: true,
	          message: 'Berhasil menambah data siswa',
	      })
    	})
    }
  })
})

router.put('/:id(\\d+)', function(request, response){
	let id = request.params.id
	const data = request.body
  const payload = request.payload
	
	Joi.validate(data, editUserSchema, (err, value) => {
    if (err) {
      response.status(422).json({
        is_ok: false,
        message: 'Invalid request data'
      })
    } else {
    	if(Object.keys(value).length === 0) {
    		response.status(500)
    			.json({
    				is_ok: false,
    				error_message: 'Parameter masih kosong'
    			})
    	} else {
	    	var sql = "UPDATE t_siswa SET"
	    	if(typeof value.nis === "string") sql += " nis = '" + value.nis + "',"
	    	if(typeof value.nama === "string") sql += " nama_lengkap = '" + value.nama + "',"
	    	if(typeof value.no_hp === "string") sql += " no_hp = '" + value.no_hp + "',"
	    	if(typeof value.tanggal_lahir === "string") sql += " tanggal_lahir = '" + value.tanggal_lahir + "',"
	    	if(typeof value.tempat_lahir === "string") sql += " tempat_lahir = '" + value.tempat_lahir + "',"

	    	sql += " last_modified = ? WHERE id = ?"

	    	let now = new Date().toISOString().slice(0, 19).replace('T', ' ')
	    	let updateCondition = [now, id]

	    	connector.query(sql, updateCondition, (err) => {
					if(err) {
						return response.status(500)
							.json({
				        is_ok: false,
				        error: err
				     })
					}
	    	})

	    	return response.status(200)
		    	.json({
	    			is_ok: true,
	    			message: "Berhasil mengubah data"
	    		})
    	}
    }
  })

})

router.delete('/:id(\\d+)', function(request, response){
	let id = request.params.id

	var sql = "SELECT * FROM t_siswa WHERE id = ?"
	let siswa = connector.query(sql, id, (err, rows) => {
		if(err) {
			return response.status(500)
				.json({
	        is_ok: false,
	        error: err
	     })
		}

		if(rows.length === 0) {
			response.status(500)
				.json({
					is_ok: false,
					error_message: "User ID tidak ditemukan!"
				})
		} else {
			var sql = "DELETE FROM t_siswa WHERE id = ?"
			let deleteSiswa = connector.query(sql, id, (err) => {
				if(err) {
					return response.status(500)
						.json({
			        is_ok: false,
			        error: err
			     })
				}
			})

			response.status(200)
				.json({
					is_ok: true,
					message: "Berhasil menghapus data siswa"
				})
		}
	})
})

module.exports = router
