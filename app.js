const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const config = require('./config')
const users = require('./routes/usersRoute')
const auth = require('./routes/authRoute')
const middlewares = require('./libs/middlewares')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Express static directory
app.use(config.STATIC_STORAGE, express.static(config.STATIC_URL))

app.get('/', function(request, response){
	response.status(200).json({
			is_ok: true,
			message: "Welcome to App"	
		})
})

app.all("*", middlewares.logger)
app.use('/auth', auth)
app.use('/users', users)

app.listen(config.PORT, function(){
   console.log('Server berjalan di port:', config.PORT)
})
